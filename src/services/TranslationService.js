import axios from "axios";

const RESOURCE_PATH = "api/translate"; //http://localhost:8081/

export default class TranslationService {
  translate(text, from, to) {
    return axios
      .post(RESOURCE_PATH, { text: text, from: from, to: to })
      .then((result) => result.data);
  }
}
